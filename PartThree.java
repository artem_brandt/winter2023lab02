import java.util.Scanner;

public class PartThree {
	public static void main (String[] args) {
		 Scanner sc = new Scanner(System.in);
		 
		 System.out.println("Enter the value of x");
		 int x = sc.nextInt();
		 System.out.println("Enter the value of y");
		 int y = sc.nextInt();
		 
		 System.out.println("Addition: " + Calculator.add(x, y));
		 System.out.println("Substraction: " + Calculator.substract(x, y));
		 Calculator cal = new Calculator();
		 System.out.println("Multiplication: " + cal.multiply(x, y));
		 System.out.println("Division: " + cal.divide(x, y));
	}
}
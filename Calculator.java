public class Calculator {
	
	public static int add(int x, int y) {
		int sum = x + y;
		return sum;
	}
	
	public static int substract(int x, int y) {
		int difference = x - y;
		return difference;
	}
	
	public int multiply(int x, int y) {
		int product = x * y;
		return product;
	}
	
	public double divide(int x, int y) {
		double quotient = x / y;
		return quotient;
	}
}
public class MethodsTest {
	public static void main (String[] args) {
		
		int x = 5;
		
		System.out.println("X in the main method: " + x);
		methodNoInputNoReturn();
		System.out.println("X in the main method: " + x);
		
		System.out.println("Before calling methodOneInputNoReturn: " + x);
		methodOneInputNoReturn(x + 10);
		System.out.println("After calling methodOneInputNoReturn: " + x);
		
		methodTwoInputNoReturn(1, 2);
		
		int y = methodNoInputReturnInt();
		System.out.println("New ineteger y after using the return method: " + y);
		
		double z = sumSquareRoot(9, 5);
		System.out.println("New double z after sumSquareRoot: " + z);
		
		String s1 = "java";
		String s2 = "programming";
		System.out.println(s1.length());
		System.out.println(s2.length());
		
		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	}
	
	public static void methodNoInputNoReturn() {
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 20;
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int x) {
		System.out.println("Inside a method One Input No Return");
		x = x - 5;
		System.out.println(x);
		
	}
	
	public static void methodTwoInputNoReturn(int x, double y) {
	System.out.println("Integer: " + x);
    System.out.println("Double: " + y);
	}
	
	public static int methodNoInputReturnInt(){
		System.out.println("Using first return method now");
		int x = 5;
		return x;
	}
	
	public static double sumSquareRoot(int x, int y) {
		double z = x + y;
		z = Math.sqrt(z);
		return z;
	}
}